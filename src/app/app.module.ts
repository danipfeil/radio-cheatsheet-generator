import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavigationComponent } from './navigation/navigation.component';

import { MatToolbarModule  } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input'; 
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { PreviewComponent } from './preview/preview.component';
import { SheetComponent } from './preview/sheet/sheet.component';
import { EditorComponent } from './editor/editor.component';
import { ReactiveFormsModule } from '@angular/forms';

// Ahead of Time Translation requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, "assets/i18n/");
}

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    PreviewComponent,
    SheetComponent,
    EditorComponent
  ],
  imports: [
    ReactiveFormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatToolbarModule,
    MatButtonModule,
    MatInputModule,
    MatIconModule,
    TranslateModule.forRoot({
      defaultLanguage: 'de',
      loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
      }
    })
  ],
  providers: [{provide: 'cheatsheets', useValue: [{
      id: 1,
      title: "Sheet1",
      entries: [
        {title: "Sheet 1 Eintrag 1"},
        {title: "Sheet 1 Eintrag 2"},
        {title: "Sheet 1 Eintrag 3"}
      ]
    },
    {
      id: 2,
      title: "Sheet2",
      entries: [
        {title: "Sheet 2 Eintrag 1"},
        {title: "Sheet 2 Eintrag 2"},
      ]
    },
  ]}],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { HttpClient, HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormArray, FormControl, ReactiveFormsModule } from '@angular/forms';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpLoaderFactory } from '../app.module';

import { EditorComponent } from './editor.component';

describe('EditorComponent', () => {
  let component: EditorComponent;
  let fixture: ComponentFixture<EditorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditorComponent ],
      imports: [ 
        ReactiveFormsModule,
        HttpClientModule,
        TranslateModule.forRoot({
          defaultLanguage: 'de',
          loader: {
              provide: TranslateLoader,
              useFactory: HttpLoaderFactory,
              deps: [HttpClient]
          }
        })
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create right amount of Input fields for entries (3 entries => 3 input fields)', () => {
    const formElement = fixture.debugElement.nativeElement.querySelector('#cheatsheetForm');
    const inputElements = formElement.querySelector('.entry-container').querySelectorAll('input');

    expect(inputElements.length).toEqual(3);
  })

  it('initial state should match the input entries', () => {
    const entryForm = component.cheatsheetForm;
    const entryFormValues = {
      title: "Titel",
      entries: [
        'Zeile 1',
        'Zeile 2',
        'Zeile 3'
      ]
    };

    expect(entryForm.value).toEqual(entryFormValues);
  })

  it('should update the value of the input field of the title field', () => {
    const input = fixture.nativeElement.querySelector('.title').querySelector('input');
    console.warn(fixture.nativeElement);
    const event = new Event('input');

    input.value = 'Testtitle';
    input.dispatchEvent(event);

    expect(fixture.componentInstance.title!.value).toEqual("Testtitle");
  })

  it('should update the value in the control of an entry field', () => {
    const titleField = component.cheatsheetForm.get('title') as FormControl;

    titleField.setValue('Testtitle');

    expect(fixture.nativeElement.querySelector('.title').querySelector('input').value).toBe('Testtitle');
  })

  it('should update the value of the input field of an entry field', () => {
    const input = fixture.nativeElement.querySelector('.entry-container').querySelector('input');
    const event = new Event('input');

    input.value = 'Testwert';
    input.dispatchEvent(event);

    expect(fixture.componentInstance.entries.value).toEqual(['Testwert', 'Zeile 2', 'Zeile 3']);
  })

  it('should update the value in the control of an entry field', () => {
    const entries = component.cheatsheetForm.get('entries') as FormArray;

    entries.controls[0].setValue('Testwert');

    expect(fixture.nativeElement.querySelector('.entry-container').querySelector('input').value).toBe('Testwert');
  })

  it('should move item up', () => {
    const entryForm = component.cheatsheetForm;
    const entryFormValues = {
      title: "Titel",
      entries: [
        'Zeile 2',
        'Zeile 1',
        'Zeile 3'
      ]
    };

    component.moveEntryUp(1);

    expect(entryForm.value).toEqual(entryFormValues);
  })

  it('should not move item up because of invalid index = 0', () => {
    const entryForm = component.cheatsheetForm;
    const entryFormValues = {
      title: "Titel",
      entries: [
        'Zeile 1',
        'Zeile 2',
        'Zeile 3'
      ]
    };

    component.moveEntryUp(0);

    expect(entryForm.value).toEqual(entryFormValues);
  })

  it('should not move item up because of invalid index < 0', () => {
    const entryForm = component.cheatsheetForm;
    const entryFormValues = {
      title: "Titel",
      entries: [
        'Zeile 1',
        'Zeile 2',
        'Zeile 3'
      ]
    };

    component.moveEntryUp(-1);

    expect(entryForm.value).toEqual(entryFormValues);
  })

  it('should move item Down', () => {
    const entryForm = component.cheatsheetForm;
    const entryFormValues = {
      title: "Titel",
      entries: [
        'Zeile 2',
        'Zeile 1',
        'Zeile 3'
      ]
    };

    component.moveEntryDown(0);

    expect(entryForm.value).toEqual(entryFormValues);
  })

  it('should not move item down because of invalid index < 0', () => {
    const entryForm = component.cheatsheetForm;
    const entryFormValues = {
      title: "Titel",
      entries: [
        'Zeile 1',
        'Zeile 2',
        'Zeile 3'
      ]
    };

    component.moveEntryDown(-1);

    expect(entryForm.value).toEqual(entryFormValues);
  })

  it('should not move item down because of invalid index larger than array', () => {
    const entryForm = component.cheatsheetForm;
    const entryFormValues = {
      title: "Titel",
      entries: [
        'Zeile 1',
        'Zeile 2',
        'Zeile 3'
      ]
    };

    component.moveEntryDown(100);

    expect(entryForm.value).toEqual(entryFormValues);
  })

  it('should add entry with empty value', () => {
    component.addEntry();

    const entryFormValues = {
      title: "Titel",
      entries: [
        'Zeile 1',
        'Zeile 2',
        'Zeile 3',
        ''
      ]
    };
    
    expect(component.entries.length).toBe(4);
    expect(component.cheatsheetForm.value).toEqual(entryFormValues);
  });

  it('should remove entry', () => {
    component.removeEntry(1);

    const entryFormValues = {
      title: "Titel",
      entries: [
        'Zeile 1',
        'Zeile 3'
      ]
    };
    
    expect(component.entries.length).toBe(2);
    expect(component.cheatsheetForm.value).toEqual(entryFormValues);
  });

  it('should update the cheatsheet corret', () => {
    component.addEntry();
    component.removeEntry(0);

    (component.cheatsheetForm.get('title') as FormControl).setValue('Testtitle');
    (component.cheatsheetForm.get('entries') as FormArray).controls[0].setValue('Testentry');

    const cheatsheet = {
      id: 1,
      title: "Testtitle",
      entries: [
        {title: 'Testentry', id: 0},
        {title: 'Zeile 3', id: 1},
        {title: '', id: 2},
      ]
    };

    component.onSubmit();

    expect(component.cheatsheet).toEqual(cheatsheet);
  })
});

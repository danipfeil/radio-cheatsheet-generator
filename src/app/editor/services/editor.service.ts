import { Inject, Injectable, Optional } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Cheatsheet } from '../../model/Cheatsheet';

@Injectable({
  providedIn: 'root'
})
export class EditorService {
  private _cheatsheet: BehaviorSubject<Cheatsheet> = new BehaviorSubject({'id': -1, title:'', entries: []} as Cheatsheet);
  public readonly cheatsheet: Observable<Cheatsheet>;

  constructor(@Optional() @Inject('activeCheatsheet') cheatsheet: Cheatsheet){
    if(cheatsheet !== null){
      this._cheatsheet.next(cheatsheet);
    }
    
    this.cheatsheet = this._cheatsheet.asObservable();
  }

  selectCheatsheet(cheatsheet: Cheatsheet){
    this._cheatsheet.next(cheatsheet);
  }
}

import { Component } from '@angular/core';
import { EditorService } from './editor/services/editor.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'radio-cheatsheet-generator';

  public editorService: EditorService;

  constructor(cheatsheetService: EditorService) {
    this.editorService = cheatsheetService;
  }
}

import { Inject, Injectable, Optional } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Cheatsheet } from '../../model/Cheatsheet';

@Injectable({
  providedIn: 'root',
})
export class CheatsheetService {
  private _cheatSheets: BehaviorSubject<Cheatsheet[]> = new BehaviorSubject([] as Cheatsheet[]);

  public readonly cheatsheets: Observable<Cheatsheet[]>;

  constructor(@Optional() @Inject('cheatsheets') cheatsheets: Cheatsheet[] = []){
    this._cheatSheets.next(cheatsheets);
    this.cheatsheets = this._cheatSheets.asObservable();
  }

  addOrReplaceCheatsheet(cheatsheet: Cheatsheet) {
    this.removeCheatsheet(cheatsheet);

    let cheatsheets = this._cheatSheets.value;

    cheatsheets.push(cheatsheet);

    this._cheatSheets.next(cheatsheets);
  }

  removeCheatsheet(cheatsheet: Cheatsheet){
    let cheatsheets = this._cheatSheets.value;

    const index = cheatsheets.findIndex((item) => {
      return item.id === cheatsheet.id;
    });
    
    if (index > -1) {
      cheatsheets.splice(index, 1);
    }

    this._cheatSheets.next(cheatsheets);
  }
}

import { Entry } from "./Entry";

export interface Cheatsheet {
  id: number;
  title: string;
  entries: Entry[];
}
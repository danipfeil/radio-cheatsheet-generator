import { Component, Input } from '@angular/core';
import { Cheatsheet } from 'src/app/model/Cheatsheet';

@Component({
  selector: 'app-preview-sheet',
  templateUrl: './sheet.component.html',
  styleUrls: ['./sheet.component.scss']
})
export class SheetComponent {
  @Input()
  sheet: Cheatsheet = {
    id: -1,
    entries: [],
    title: ""
  };
}
